package br.com.mastertech.emissaonfe.solicitacao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "processanfe")
public interface NfeClient {

    @GetMapping("/processanfe/buscar/{nfeId}")
    public Object buscaNfe(@PathVariable int nfeId);
}
