package br.com.mastertech.emissaonfe.solicitacao.Dto;

public class SolicitacaoRequest {

    private String identidade;
    private double valor;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
