package br.com.mastertech.emissaonfe.solicitacao.service;

import br.com.mastertech.emissaonfe.model.LogSolicitacao;
import br.com.mastertech.emissaonfe.model.SolicitacaoKafkaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

    @Autowired//       (tipo chave, tipo valor)
    private KafkaTemplate<String, SolicitacaoKafkaRequest> sender;

    @Autowired//       (tipo chave, tipo valor)
    private KafkaTemplate<String, LogSolicitacao> senderLog;

    public void enviarValorNfe(SolicitacaoKafkaRequest solicitacao) {
        //         (topico, chave, valor)
        sender.send("leandro-biro-1", "4", solicitacao);
    }

    public void enviarLogSolicitacao(LogSolicitacao logSolicitacao) {
        //         (topico, chave, valor)
        senderLog.send("leandro-biro-2", "1", logSolicitacao);
    }
}