package br.com.mastertech.emissaonfe.solicitacao.service;

import br.com.mastertech.emissaonfe.solicitacao.client.NfeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NfeService {

    @Autowired
    NfeClient nfeClient;

    public Object buscaNfe(int nfeId) {
        return nfeClient.buscaNfe(nfeId);
    }
}
