package br.com.mastertech.emissaonfe.model;

public class SolicitacaoKafkaRequest {
    private int solicitacaoId;
    private String identidade;
    private Double valor;
    private Object nfe;

    public int getSolicitacaoId() {
        return solicitacaoId;
    }

    public void setSolicitacaoId(int solicitacaoId) {
        this.solicitacaoId = solicitacaoId;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Object getNfe() {
        return nfe;
    }

    public void setNfe(Object nfe) {
        this.nfe = nfe;
    }
}
