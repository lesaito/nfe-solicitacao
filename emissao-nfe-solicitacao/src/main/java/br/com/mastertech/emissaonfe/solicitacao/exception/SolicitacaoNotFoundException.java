package br.com.mastertech.emissaonfe.solicitacao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Solicitacao de Nfe não encontrada")
public class SolicitacaoNotFoundException extends RuntimeException {
}
