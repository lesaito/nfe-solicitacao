package br.com.mastertech.emissaonfe.solicitacao.mapper;

import br.com.mastertech.emissaonfe.model.Solicitacao;
import br.com.mastertech.emissaonfe.solicitacao.Dto.SolicitacaoRequest;
import br.com.mastertech.emissaonfe.solicitacao.Dto.SolicitacaoResponse;

public class SolicitacaoMapper {

    public static Solicitacao toSolicitacao(SolicitacaoRequest solicitacaoNFeRequest, String status) {
        Solicitacao solicitacaoNFe = new Solicitacao();
        solicitacaoNFe.setIdentidade(solicitacaoNFeRequest.getIdentidade());
        solicitacaoNFe.setValor(solicitacaoNFeRequest.getValor());
        solicitacaoNFe.setStatus(status);

        return solicitacaoNFe;
    }

    public static SolicitacaoResponse toSolicitacaoResponse(Solicitacao solicitacao, Object nfe) {
        SolicitacaoResponse solicitacaoResponse = new SolicitacaoResponse();
        solicitacaoResponse.setIdentidade(solicitacao.getIdentidade());
        solicitacaoResponse.setValor(solicitacao.getValor());
        solicitacaoResponse.setStatus(solicitacao.getStatus());
        solicitacaoResponse.setNfe(nfe);

        return solicitacaoResponse;
    }
}
