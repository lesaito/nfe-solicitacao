package br.com.mastertech.emissaonfe.solicitacao.Dto;

public class SolicitacaoResponse {

    private String identidade;
    private double valor;
    private String status;
    private Object nfe;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getNfe() {
        return nfe;
    }

    public void setNfe(Object nfe) {
        this.nfe = nfe;
    }
}
