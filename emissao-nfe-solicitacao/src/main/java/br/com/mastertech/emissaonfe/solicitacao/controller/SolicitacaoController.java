package br.com.mastertech.emissaonfe.solicitacao.controller;

import br.com.mastertech.emissaonfe.model.LogSolicitacao;
import br.com.mastertech.emissaonfe.model.Solicitacao;
import br.com.mastertech.emissaonfe.model.Usuario;
import br.com.mastertech.emissaonfe.model.SolicitacaoKafkaRequest;
import br.com.mastertech.emissaonfe.solicitacao.Dto.SolicitacaoRequest;
import br.com.mastertech.emissaonfe.solicitacao.Dto.SolicitacaoResponse;
import br.com.mastertech.emissaonfe.solicitacao.mapper.SolicitacaoMapper;
import br.com.mastertech.emissaonfe.solicitacao.service.KafkaService;
import br.com.mastertech.emissaonfe.solicitacao.service.NfeService;
import br.com.mastertech.emissaonfe.solicitacao.service.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/nfe")
public class SolicitacaoController {

    private final static String STATUS_PENDING = "pending";
    private final static String STATUS_COMPLETE = "complete";
    private static final String SOLICITACAO_EMISSAO = "Emissao";
    private static final String SOLICITACAO_CONSULTA = "Consulta";


    @Autowired
    private KafkaService kafkaService;

    @Autowired
    private SolicitacaoService solicitacaoService;

    @Autowired
    private NfeService nfeService;

    @PostMapping("/solicitar")
    public ResponseEntity<SolicitacaoResponse> emitir(@AuthenticationPrincipal Usuario usuario, @RequestBody SolicitacaoRequest solicitacaoRequest) {
        Solicitacao solicitacaoNFe = SolicitacaoMapper.toSolicitacao(solicitacaoRequest, STATUS_PENDING);
        solicitacaoNFe = solicitacaoService.salva(solicitacaoNFe);
        //envia solicitacao ao Kafka
        SolicitacaoKafkaRequest solicitacaoKafkaRequest = new SolicitacaoKafkaRequest();
        solicitacaoKafkaRequest.setSolicitacaoId(solicitacaoNFe.getId());
        solicitacaoKafkaRequest.setIdentidade(solicitacaoNFe.getIdentidade());
        solicitacaoKafkaRequest.setValor(solicitacaoNFe.getValor());
        kafkaService.enviarValorNfe(solicitacaoKafkaRequest);

        //enviar log de acesso a um microservico de registro de log
        LogSolicitacao logSolicitacao = new LogSolicitacao();
        logSolicitacao.setHorario(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy-hh:mm:ss")));
        logSolicitacao.setIdentidade(solicitacaoRequest.getIdentidade());
        logSolicitacao.setTipoSolicitacao(SOLICITACAO_EMISSAO);
        logSolicitacao.setValor(solicitacaoRequest.getValor());
        kafkaService.enviarLogSolicitacao(logSolicitacao);

        return new ResponseEntity<SolicitacaoResponse>(SolicitacaoMapper.toSolicitacaoResponse(solicitacaoNFe, null), HttpStatus.OK);
    }

    @GetMapping("/consultar/{identidade}")
    @ResponseStatus(HttpStatus.OK)
    public List<SolicitacaoResponse> consultar(@PathVariable(name = "identidade") String identidade) {
        //consulta a solicitacao
        List<SolicitacaoResponse> listaResponse = new ArrayList<>();
        List<Solicitacao> listaSolicitacao = solicitacaoService.lista(identidade);

        //para cada solicitacao, monta a lista
        for(Solicitacao solicitacao : listaSolicitacao) {
            SolicitacaoResponse solicitacaoResponse = new SolicitacaoResponse();
            solicitacaoResponse.setIdentidade(solicitacao.getIdentidade());
            solicitacaoResponse.setStatus(solicitacao.getStatus());
            solicitacaoResponse.setValor(solicitacao.getValor());

            //solicitao efetivada, busca a NFe correspondente
            if(solicitacao.getStatus().equals(STATUS_COMPLETE)) {
                Object nfe = nfeService.buscaNfe(solicitacao.getNfeId());
                solicitacaoResponse.setNfe(nfe);
            }

            listaResponse.add(solicitacaoResponse);
        }

        //envia log
        LogSolicitacao logSolicitacao = new LogSolicitacao();
        logSolicitacao.setHorario(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy-hh:mm:ss")));
        logSolicitacao.setIdentidade(identidade);
        logSolicitacao.setTipoSolicitacao(SOLICITACAO_CONSULTA);
        kafkaService.enviarLogSolicitacao(logSolicitacao);
        return listaResponse;
    }

}



