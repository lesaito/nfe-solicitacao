package br.com.mastertech.emissaonfe.model;

import javax.persistence.*;

@Table
@Entity
public class Solicitacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String identidade;
    @Column
    private Double valor;
    @Column
    private String Status;
    @Column
    private int nfeId;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNfeId() {
        return nfeId;
    }

    public void setNfeId(int nfeId) {
        this.nfeId = nfeId;
    }
}
