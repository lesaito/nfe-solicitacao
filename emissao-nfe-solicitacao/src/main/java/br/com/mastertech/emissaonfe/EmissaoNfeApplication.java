package br.com.mastertech.emissaonfe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EmissaoNfeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmissaoNfeApplication.class, args);
	}

}
