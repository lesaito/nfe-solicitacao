package br.com.mastertech.emissaonfe.solicitacao.controller;

import br.com.mastertech.emissaonfe.model.Solicitacao;
import br.com.mastertech.emissaonfe.model.SolicitacaoKafkaRequest;
import br.com.mastertech.emissaonfe.solicitacao.service.SolicitacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaController {

    private final static String STATUS_COMPLETE = "complete";

    @Autowired
    SolicitacaoService solicitacaoService;

    @KafkaListener(topics = "leandro-biro-1", groupId = "5")
    public void recebeNfe(@Payload SolicitacaoKafkaRequest solicitacaoKafkaRequest) {
        Solicitacao solicitacao = solicitacaoService.busca(solicitacaoKafkaRequest.getSolicitacaoId());
        solicitacao.setNfeId(solicitacaoKafkaRequest.getSolicitacaoId());
        solicitacao.setStatus(STATUS_COMPLETE);
        solicitacaoService.salva(solicitacao);

        System.out.println("Recebeu mensagem NFe do " + solicitacaoKafkaRequest.getIdentidade() +
                " com valor de " + solicitacao.getValor());
    }

}