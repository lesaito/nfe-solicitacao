package br.com.mastertech.emissaonfe.solicitacao.service;

import br.com.mastertech.emissaonfe.model.Solicitacao;
import br.com.mastertech.emissaonfe.solicitacao.exception.SolicitacaoNotFoundException;
import br.com.mastertech.emissaonfe.solicitacao.repository.SolicitacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SolicitacaoService {

    @Autowired
    SolicitacaoRepository solicitacaoRepository;

    public Solicitacao salva(Solicitacao solicitacaoNFe) {
        return solicitacaoRepository.save(solicitacaoNFe);
    }

    public Solicitacao busca(int solicitacaoId) {
        Optional<Solicitacao> solicitacao = solicitacaoRepository.findById(solicitacaoId);
        if(solicitacao.isPresent()) {
            return solicitacao.get();
        } else {
            throw new RuntimeException("Solicitacao nao encontrada");
        }
    }

    public List<Solicitacao> lista(String identidade) {
        List<Solicitacao> listaSolicitacao = solicitacaoRepository.findAllByIdentidade(identidade);
        if (listaSolicitacao.isEmpty()) {
            throw new SolicitacaoNotFoundException();
        } else {
            return listaSolicitacao;
        }


    }
}
