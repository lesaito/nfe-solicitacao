package br.com.mastertech.emissaonfe.solicitacao.repository;

import br.com.mastertech.emissaonfe.model.Solicitacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SolicitacaoRepository extends CrudRepository<Solicitacao, Integer> {

    public List<Solicitacao> findAllByIdentidade(String identidade);
}
